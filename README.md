# Tocco IDEA settings

This repository contains Tocco specific IDEA settings.

At this stage, this repository contains only Live Templates.

## Tocco Live Templates

### Available Live Templates

| Abbreviation | Description                     | Context |
|--------------|---------------------------------|---------|
| dp           | Java Unit Test Data Provider    | Java    |
| jbefc        | Empty Java @BeforeClass Method  | Java    |
| jbefm        | Empty Java @BeforeMethod Method | Java    |
| jtest        | Empty Java Unit Test Method     | Java    |
| tx           | Empty Transaction Invoker       | Java    |


### Import the Tocco Live Templates

Download the zip file with the settings from [here](https://toccoag.gitlab.io/idea-settings/settings.zip) or build it yourself:

```
cd settings && zip -r settings.zip .
```

Import the ZIP archive in your IDE:
1. On the File Menu, click Import Settings.
2. Select the created ZIP archive (`settings/settings.zip` in this repository).
3. In the Import Settings dialog box, selet the Live templates checkbox and click OK.
4. After restarting IntelliJ IDEA, you will see the imported live templates on the Live Templates page of the
   Settings / Preferences Dialog. The template group is called "Tocco".


### Apply changes in the Tocco Live Templates

1. After having imported the Live Templates following the instructions above, apply the desired changes directly in
   the IDE. You can also remove a Live Template or add a new one.
2. Once all desired changes are applied, export the settings.
    1. On the File menu, click Export Settings.
    2. In the Export Settings dialog, make sure that the Live templates check box is selected and specify the path and
       name of the JAR file, where the exported settings will be saved.
    3. Click OK to generate the file based on live template configuration files.
3. Apply the settings to this repository
    1. Find the generated JAR archive and extract its contents.
    2. Copy the extracted content into the `settings` directory of this repository.


> See https://www.jetbrains.com/help/idea/sharing-live-templates.html for more information.
